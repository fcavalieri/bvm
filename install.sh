#!/usr/bin/env bash

{ # this ensures the entire script is downloaded #

bvm_has() {
  type "$1" > /dev/null 2>&1
}

bvm_echo() {
  command printf %s\\n "$*" 2>/dev/null
}

if [ -z "${BASH_VERSION}" ] || [ -n "${ZSH_VERSION}" ]; then
  # shellcheck disable=SC2016
  bvm_echo >&2 'Error: the install instructions explicitly say to pipe the install script to `bash`; please follow them'
  exit 1
fi

bvm_grep() {
  GREP_OPTIONS='' command grep "$@"
}

bvm_install_dir() {
  if [ -n "$BVM_DIR" ]; then
    printf %s "${BVM_DIR}"
  else
    printf "%s" "${HOME}/.local/share/beeper"
  fi
}

bvm_download() {
  if bvm_has "curl"; then
    curl --fail --compressed -q "$@"
  elif bvm_has "wget"; then
    # Emulate curl with wget
    ARGS=$(bvm_echo "$@" | command sed -e 's/--progress-bar /--progress=bar /' \
                            -e 's/--compressed //' \
                            -e 's/--fail //' \
                            -e 's/-L //' \
                            -e 's/-I /--server-response /' \
                            -e 's/-s /-q /' \
                            -e 's/-sS /-nv /' \
                            -e 's/-o /-O /' \
                            -e 's/-C - /-c /')
    # shellcheck disable=SC2086
    eval wget $ARGS
  fi
}

install_bvm_as_script() {
  local INSTALL_DIR
  INSTALL_DIR="$(bvm_install_dir)"
  local BVM_SCRIPT_SOURCE
  BVM_SCRIPT_SOURCE="https://gitlab.com/fcavalieri/bvm/-/raw/main/beeper.sh?ref_type=heads"  
  
  # Downloading to $INSTALL_DIR  
  if [ -f "$INSTALL_DIR/bvm.sh" ]; then
    bvm_echo "=> bvm is already installed in $INSTALL_DIR, trying to update the script"
  else
    bvm_echo "=> Downloading bvm as script to '$INSTALL_DIR'"
  fi
  mkdir -p "$INSTALL_DIR"  
  bvm_download -s "$BVM_SCRIPT_SOURCE" -o "$INSTALL_DIR/beeper.sh" || {
    bvm_echo >&2 "Failed to download '$BVM_SCRIPT_SOURCE'"
    return 1
  } &
  for job in $(jobs -p | command sort)
  do
    wait "$job" || return $?
  done
  bvm_echo "=> Downloaded bvm as script to '$INSTALL_DIR'"
  chmod a+x "$INSTALL_DIR/beeper.sh" || {
    bvm_echo >&2 "Failed to mark '$INSTALL_DIR/beeper.sh' as executable"
    return 3
  }

SHORTCUT="
[Desktop Entry]
Comment=
Exec=$INSTALL_DIR/beeper.sh
Name=Beeper
NoDisplay=false
Path=
StartupNotify=true
Terminal=false
TerminalOptions=
Type=Application
X-KDE-SubstituteUID=false
X-KDE-Username=
MimeType=x-scheme-handler/element;
"

echo "$SHORTCUT" > "$HOME/.local/share/applications/Beeper.desktop"

}

bvm_do_install() {
  if [ -n "${BVM_DIR-}" ] && ! [ -d "${BVM_DIR}" ]; then
    if [ -e "${BVM_DIR}" ]; then
      bvm_echo >&2 "File \"${BVM_DIR}\" has the same name as installation directory."
      exit 1
    fi

    if [ "${BVM_DIR}" = "$(bvm_default_install_dir)" ]; then
      mkdir "${BVM_DIR}"
    else
      bvm_echo >&2 "You have \$BVM_DIR set to \"${BVM_DIR}\", but that directory does not exist. Check your profile files and environment."
      exit 1
    fi
  fi

  if bvm_has curl || bvm_has wget; then
      install_bvm_as_script
  else
    bvm_echo >&2 'You need curl, or wget to install dvm'
    exit 1
  fi
  
  bvm_echo
  
  bvm_reset
}

#
# Unsets the various functions defined
# during the execution of the install script
#
bvm_reset() {
  unset -f bvm_has bvm_install_dir bvm_latest_version bvm_profile_is_bash_or_zsh \
    bvm_source bvm_node_version bvm_download install_bvm_from_git bvm_install_node \
    install_bvm_as_script bvm_try_profile bvm_detect_profile bvm_check_global_modules \
    bvm_do_install bvm_reset bvm_default_install_dir bvm_grep
}

bvm_do_install

} # this ensures the entire script is downloaded #
