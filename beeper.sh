#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
CURRENT_FILE=$(find $SCRIPT_DIR -type f -name "*.AppImage")
if [[ ! -n $CURRENT_FILE ]]
then
  echo "Downloading Beeper"
  wget --content-disposition https://download.beeper.com/linux/appImage/x64
fi
CURRENT_FILE=$(find $SCRIPT_DIR -type f -name "*.AppImage")
if [[ -n $CURRENT_FILE ]]
then
  echo "Running $CURRENT_FILE"
  chmod +x $CURRENT_FILE
  exec $CURRENT_FILE
fi
